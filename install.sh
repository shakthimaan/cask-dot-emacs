#!/bin/bash
# TODO
# export org-latex problem

if [ ! -e ~/.cask ]
then
    echo "Cloning Cask repo"
    git clone https://github.com/cask/cask.git ~/.cask
fi

if [ ! -e ~/.bashrc ] || [ $(grep -c "cask/bin" ~/.bashrc) -eq 0 ]
then
    echo "Adding \$HOME/.cask/bin to \$PATH in ~/.bashrc"
    echo '' >> ~/.bashrc
    echo "# Added by ~/.emacs.d/install.sh" >> ~/.bashrc
    echo "export PATH=\$HOME/.cask/bin:\$PATH" >> ~/.bashrc
fi

export PATH=$HOME/.cask/bin:$PATH

cask install
