;; NOTE: The name of the Org files is important!  When a file gets tangled,
;; it gets the same base name as the Org file.  Thus, tangling Emacs Lisp from
;; a file `init.org` would generate `init.el`, obliterating this file in the
;; process. So your config org file should not be named "init.org".

(package-initialize)

;; (load "~/.emacs.d/init.el")

;; Initialize cask to get the correct version of org-mode
(require 'cask "~/.cask/cask.el")
(cask-initialize)

(require 'ob-tangle)
;; (setq debug-on-error t)
(org-babel-load-file
 (expand-file-name "emacs-init.org"
                   user-emacs-directory))

;; Package Manager
;; See ~Cask~ file for its configuration
;; https://github.com/cask/cask

;; (require 'cask "~/.cask/cask.el")
;; (cask-initialize)

;; (set-language-environment "UTF-8")
;; (set-default-coding-systems 'utf-8)

;; Create a variable to store the path to this dotfile directory
;; (Usually ~/.emacs.d)

;; (setq dotfiles-dir (file-name-directory
;;                     (or (buffer-file-name) load-file-name)))

;; Create variables to store the path to this dotfile dir's lib etc and tmp directories
;; (setq dotfiles-lib-dir (concat dotfiles-dir "lib/"))
;; (setq dotfiles-tmp-dir (concat dotfiles-dir "tmp/"))
;; (setq dotfiles-etc-dir (concat dotfiles-dir "etc/"))

;; Create helper functions for loading dotfile paths and files
;; (defun add-dotfile-path (p)
;;   (add-to-list 'load-path (concat dotfiles-dir p)))

;; (defun load-dotfile (f)
;;   (load-file (concat dotfiles-dir f)))

;; Ensure the etc directory is on the load path
;; (add-dotfile-path "etc")
;; (add-dotfile-path "lib")

;; (load-dotfile "lib/scrum.el")
;; (load-dotfile "lib/no-easy-keys.el")
;; (load-dotfile "lib/org-depend.el")
;; (load-dotfile "lib/tamil-dvorak.el")

;; (load-dotfile "etc/custom.el")
;; (load-dotfile "etc/init-global.el")
;; (load-dotfile "etc/init-org.el")
;; (load-dotfile "etc/init-magit.el")
;; (load-dotfile "etc/init-elfeed.el")
;; (load-dotfile "etc/init-winner.el")
;; (load-dotfile "etc/init-rcirc.el")
;; (load-dotfile "etc/init-ace-window.el")
;; (load-dotfile "etc/init-multiple-cursors.el")
;; (load-dotfile "etc/init-eshell.el")
;; (load-dotfile "etc/init-restclient.el")
;; (load-dotfile "etc/init-workgroups.el")
;; (load-dotfile "etc/init-window.el")
;; (load-dotfile "etc/init-find.el")
;; (load-dotfile "etc/init-lisp.el")
;; (load-dotfile "etc/init-js.el")
;; (load-dotfile "etc/init-helm.el")

;; (server-start)
